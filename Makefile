.PHONY: all build clean read

FILENAME=
TEX=pdflatex
BIBTEX=bibtex
BUILDTEX=$(TEX) $(FILENAME).tex

%.pdf: %.tex
	$(TEX) $*.tex
	$(TEX) $*.tex
	$(TEX) $*.tex
	#make clean

all: build clean

build:
	$(BUILDTEX)
	$(BIBTEX) $(FILENAME)
	$(BUILDTEX)
	$(BUILDTEX)

read:
	zathura $(FILENAME).pdf

clean:
	rm -f *.acn
	rm -f *.acr
	rm -f *.alg
	rm -f *.aux
	rm -f *.bbl
	rm -f *.blg
	rm -f *.dvi
	rm -f *.fdb_latexmk
	rm -f *.glg
	rm -f *.glo
	rm -f *.gls
	rm -f *.idx
	rm -f *.ilg
	rm -f *.ind
	rm -f *.ist
	rm -f *.lof
	rm -f *.log
	rm -f *.lol
	rm -f *.lot
	rm -f *.maf
	rm -f *.mtc
	rm -f *.mtc0
	rm -f *.nav
	rm -f *.nlo
	rm -f *.out
	rm -f *.pdfsync
	rm -f *.ps
	rm -f *.snm
	rm -f *.swp
	rm -f *.synctex.gz
	rm -f *.toc
	rm -f *.vrb
	rm -f *.xdy
	rm -f *.tdo
