\subsection{Software FRDM}
\label{sub:software_frdm}

Wie im Konzept aus PREN1 beschrieben, wird ein Teil der Logik der Gesamtlösung auf einem Mikrocontroller
implementiert. Dies hat den Vorteil, dass dadurch eine hardwarenahe Lösung entsteht. Dank der
exakt einstellbaren Clock-Zeiten, kann eine Echtzeit-Anwendung umgesetzt werden. Diese eignet sich
optimal für Regelungen. Um ein möglichst hohes Abstraktionslevel zu erhalten, wurden
grosse Teile der Logik und logischen Abläufe hier umgesetzt. Schlussendlich erhalten wir
eine simple Schnittstelle, über welche das Raspberry mittels wenigen Befehlen die Navigation
übernehmen kann.

\subsubsection{Überblick}
\label{ssub:_berblick}

\begin{figure}[htpb]
  \centering
  \includegraphics[width=1.1\linewidth]{frdm-programmteile.png}
  \caption{Programmteile des FRMD}
  \label{fig:frdm-programmteile}
\end{figure}

In Abbildung \namerefit{fig:frdm-programmteile} ist ein Überblick der implementierten Programmteile gegeben.
Die Grafik wurde während des Entwicklungsprozesses laufend angepasst.

\paragraph{main.c}
\label{par:main_c}

Diese Datei stellt den Programm-Einstiegspunkt dar. Sie wurde nicht in die vorangehende Grafik
eingebunden, weil sie automatisch vom Processor-Experten
generiert wird. Die einzige Anpassung, welche vorgenommen wurde, ist der Aufruf einer Funktion von
Application.c. Dieser Aufruf bewirkt den Start der Applikation.

\paragraph{Application.c}
\label{par:application_c}

In diesem Dokument werden alle Initialisierungen vorgenommen. Als Abschluss
der Initialisierung wird das Echtzeit-Betriebssystem
gestartet.

Der Scheduler verteilt die CPU-Zeit nun nach dem pre-emptive Konzept. Das heisst,
der Task mit der höchsten Priorität wird als erstes ausgeführt.

Auf die andern Programmteile wird in den folgenden Kapiteln eingegangen.

\subsubsection{Processor Expert Komponenten}
\label{ssub:processor_expert_komponenten}

\begin{figure}[htpb]
  \centering
  \includegraphics[width=0.6\linewidth]{frdm-ppg.png}
  \caption{Programmierbare Puls Generator}
  \label{fig:frdm-ppg}
\end{figure}

\begin{figure}[htpb]
  \centering
  \includegraphics[width=0.6\linewidth]{bluetooth.png}
  \caption{Bluetooth-Kommunikation}
  \label{fig:bluetooth}
\end{figure}

\begin{figure}[htpb]
  \centering
  \includegraphics[width=0.6\linewidth]{frdm-sensors.png}
  \caption{Steuerung der Sensoren}
  \label{fig:frdm-sensors}
\end{figure}

Der programmierbare Puls Generator (PPG) (im Bild \namerefit{fig:frdm-ppg} \texttt{motorLeft} und
\texttt{motorRight}) wird verwendet, um die Motortreiber der Schrittmotoren
anzusteuern. Mit NSLEEP können die Motoren deaktiviert
werden. Diese Funktion wird beim Stillstand verwendet. Ansonsten würden durch die Wicklungen des
Motors ständig einen Strom fliessen und diese stark
erhitzen.

Um die Servos anzusteuern, wird eine Timer-Unit mit vier Channels
verwendet. Diese Channels werden auf Output-Compare
geschaltet. Um den Winkel eines Servos zu ändern, kann einfach der ChannelValue angepasst werden.


Die Fehlersuche und das Einstellen des PID-Reglers während des Betriebs wurde über eine
Bluetooth-Schnittstelle realisiert. Diese Komponenten sind im Bild \namerefit{fig:bluetooth}
ersichtlich.

Ebenfalls dort sieht man die serielle Schnittstelle, mit welcher mit dem Raspberry Pi kommuniziert
wird. Diese wird im Kapitel \namerefit{sub:itserial} beschrieben.

\begin{figure}[htpb]
  \centering
  \includegraphics[width=0.5\linewidth]{frdm-visualization.png}
  \caption{LEDs für den Programmstatus}
  \label{fig:frdm-visualization}
\end{figure}

\begin{figure}[htpb]
  \centering
  \includegraphics[width=0.5\linewidth]{frdm-toolbox.png}
  \caption{Verschiedene Komponente}
  \label{fig:frdm-toolbox}
\end{figure}


In der Grafik \namerefit{fig:frdm-sensors} sind die Komponenten ersichtlich, die für das Ansteuern der
verschiedenen Sensoren zuständig sind.

\begin{description}
  \item[Farbsensor] Über I2C kann mit dem Farbsensor kommuniziert werden. Da die On-Board LED die
    Farbmessung stört, wird sie ausgeschaltet. Dies geschieht indem der RGB\_Sensor\_LED auf GND
    geschaltet wird.
  \item[LED] Um den Container genügend auszuleuchten, können über die zwei LED\_Enable insgesamt 8
    weisse LEDs eingeschaltet werden.
  \item[Analoge Signale] Mittels eines Analog-Digital-Wandler können die Signale des Infrarot
    Sensors und das der Batterie-Spannungs-Überwachung gemessen werden.
\end{description}


Um den Programm-Status zu visualisieren werden diverse LEDs angesteuert. Die ersten beiden
befinden sich auf dem FRDM-Board, die anderen wurden auf dem Main-Board montiert.
Die Komponenten, die dazu nötig sind, sieht man auf dem Bild \namerefit{fig:frdm-visualization}.


In der Grafik \namerefit{fig:frdm-toolbox} werden alle Komponenten aufgelistet, welche von diversen
anderen, übergeordneten Programmteilen verwendet werden.

\subsubsection{Antrieb}
\label{ssub:antrieb}

\paragraph{Ansteuerung}
\label{par:ansteuerung}

Um die beiden Schrittmotoren anzusteuern, müssen zwei Signale generiert werden, bei welchen die
Perioden-Dauer unabhängig verstellbar ist. Sie soll zwischen 500 und 65 000 µs verstellbar sein.
Die Zeit, in welcher die Ausgänge auf ‚1‘ sind, bleibt immer auf 200 µs. Bei einem Raddurchmesser
von 60 mm können damit Geschwindigkeiten von ca. 1 bis 250 $\sfrac{mm}{s}$ erreicht werden. Diese Anforderung
kann mit folgenden zwei Methoden gelöst werden.

\noindent\textbf{1. Value-Wert erhöhen}

Mithilfe von output-compare wird ein Interrupt generiert. Wird der ChannelValue erreicht, addiert
man zum aktuellen Counter-Wert eine Zeit $T$. Diese Variable $T$ kann nun verkleinert
und somit die Periodendauer verkürzt werden.

Der Vorteil dabei ist, dass nur zwei Channels benötigt werden.

Der Nachteil dagegen ist der folgende: Pro Periode (Step) werden zwei Interrupts
ausgelöst. Für eine volle Umdrehung werden 200 Fullsteps, bzw. 1600 $\sfrac{1}{8}$-Steps
benötigt.
Um die benötigte Geschwindigkeit zu erreichen (eine Umdrehung pro Sekunde), müssten pro
Motor 400, bzw. 3200 Interrupts pro Sekunde generiert werden. Da zwei Schrittmotoren im $\sfrac{1}{8}$-Step
Modus benötigt werden, würden alleine deren Ansteuerung Interrupts mit 6.4 kHz auslösen. Dies
bedeutet, dass jeder dieser Interrupts innerhalb von 156 µs abgearbeitet werden muss. Das alleine
wäre kein Problem. Da aber noch diverse andere Interrupts auftreten können, könnte das gesamte
System zu sehr belastet werden.


\noindent\textbf{2. Modulo-Wert verändern}

Eine zweite Möglichkeit besteht darin, die Periodendauer direkt mit dem Modulo-Wert zu verändern.
Da kein Interrupt bei einem Step ausgelöst wird, ist es nicht möglich, die Schritte exakt zu
zählen. Die Herausforderung besteht darin, auch bei einer Beschleunigung eine exakte Wegmessung zu
garantieren.

Mit dieser Methode gibt es deutlich weniger Interrupts.

Dafür wird pro Motor ein Timer benötigt. Da auf dem FRDM-Board nur drei vorhanden sind, besteht
hier ein Engpass. Timer 1 \& 2 werden für die Schrittmotoren verwendet. Da die
Periodendauer (Modulo-Wert) bei höchster Geschwindigkeit (200 $\sfrac{mm}{s}$) auf 0.2 ms abfällt, können
diese Timer kaum für andere Anwendungen verwendet werden. Da Timer 1 \& 2 jeweils zwei Channel
besitzen, und nur einer verwendet werden kann, \glqq verlieren\grqq{} hier also zwei Channel.

Um allfällige Probleme mit dem RTOS zu vermeiden, welche durch zu viele Interrupts auftreten
könnten, wurde die Methode mit dem Anpassen des Modulo-Wertes umgesetzt. Dieses Anpassen wird
exakt alle 10 ms durchgeführt. Es werden immer während 10 ms mit der gleichen Frequenz Schritte
ausgeführt. Nur so ist eine lineare Beschleunigung realisierbar.

Um die Perioden-Zeiten zu berechnen wurde folgende Funktion hinterlegt. Wobei $T_i$ eine diskrete
Zeit aus einer Reihe darstellt. $T_{i-1}$ ist die Zeit der letzten 10 ms. Die Parameter $a, b$ und $c$
beschreiben das Verhalten der Funktion und wurden experimentell auf einen Wert von 2 festgelegt.
In der Variable $i$ wird abgespeichert, wie weit beschleunigt wurde.

\begin{equation*}
  T_i = T_{i-1} \pm \left[\frac{a \times T_{i-1}}{b \times i + c}\right] \\
  \text{mit } a = 2; b = 2; c = 2
\end{equation*}

%Mit den vorhin definierten Parametern ergibt sich eine Beschleunigung von ca. $0.2 \sfrac{m}{m^2}$. Bei einer
Mit den vorhin definierten Parametern ergibt sich eine Beschleunigung von ca. $0.2$ $\rfrac{m}{m^2}$. Bei einer
%Beschleunigung auf $200$ $\sfrac{mm}{s}$  werden in einer Sekunde beinahe 950 Schritte bzw. 110 mm zurückgelegt.
Geschwindigkeit auf $200$ $ \rfrac{mm}{s^2}$  werden in einer Sekunde beinahe 950 Schritte bzw. 110 mm zurückgelegt.

\begin{figure}[htpb]
  \centering
  \includegraphics[width=0.8\linewidth]{geschwindigkeit-zeit-diagramm.png}
  \caption{Geschwindigkeit pro Zeit}
  \label{fig:velocitydiagram}
\end{figure}

Es ist nun möglich, die Zielgeschwindigkeit jederzeit zu verändern. Je nach aktueller
Geschwindigkeit, wird ein State-Wechsel vorgenommen. Auf die State-Machine wird im Abschnitt
\namerefit{par:fsm_motioncontroller} genauer eingegangen.


\paragraph{Weglängen-Messung}
\label{par:wegl_ngen_messung}

Gemäss den Anforderungen im Konzept wurde ein Weglängen-Messsystem entwickelt. Wie im vorherigen
Kapitel beschrieben, ist es nicht möglich die Anzahl der Schritte zu zählen. Die Lösung besteht
darin, diese Anzahl mit folgender Funktion zu errechnen.

\begin{equation*}
  n = \frac{\Delta t}{T}
\end{equation*}

Diese Operation wird einmal pro $\Delta t$ (10 ms) ausgeführt und das Ergebnis $n$ der Variable
\texttt{StepCount} hinzuaddiert. $T$ stellt die aktuelle Perioden-Zeit dar.

\paragraph{FSM MotionController}
\label{par:fsm_motioncontroller}

Um eine einwandfreie Bewegung des Fahrzeuges zu garantieren, wurden alle Funktionen der
Schrittmotoren mit einer Statemachine gesteuert. Damit ist es möglich, jederzeit (alle 10 ms) die
Geschwindigkeit und (alle 50 ms) die Steuerung anzupassen. Diese Definitionen sind notwendig um
eine saubere Regelung zu implementieren.

\noindent\textbf{Beschleunigung}

In einer Struktur werden die aktuelle und die Zielgeschwindigkeit abgespeichert. Um
Rechenaufwand zu ersparen, wird in der Software die Geschwindigkeit als Periodenzeiten definiert.
Die Zielgeschwindigkeit kann jederzeit vom Raspberry über UART oder vom Anwender über Bluetooth
angepasst werden. Sobald die aktuelle Geschwindigkeit von der Zielgeschwindigkeit abweicht, wird
ein State-Wechsel vorgenommen, wie in der Abbildung \namerefit{fig:fsm-motioncontroller} ersichtlich.

\begin{figure}[htpb]
  \centering
  \includegraphics[width=0.8\linewidth]{fsm-motioncontroller.png}
  \caption{Statemachine für die Beschleunigung}
  \label{fig:fsm-motioncontroller}
\end{figure}

\noindent\textbf{Lenkung}

Mit der oben beschriebenen State-Machine wird die Durchschnittsgeschwindigkeit definiert. Dies ist
die Geschwindigkeit, mit welcher sich der Punkt in der Mitte zwischen den beiden Schrittmotoren
bewegt. Mit der Funktion \texttt{MOT\_Steer()} werden die genauen Perioden-Zeiten der beiden Motoren
gesetzt. In der besagten Struktur wird zusätzlich ein Wert \texttt{steeringLock} (Steuereinschlag)
abgespeichert. Dieser entspricht dem Prozentsatz, welcher der Geschwindigkeit des rechten Rades
hinzuaddiert bzw. der des linken Rades subtrahiert. In der Abbildung \namerefit{fig:lenkung} ist ein Beispiel einer
Lenkung von 25\% aufgezeigt. Die Geschwindigkeiten sind als Vektoren dargestellt.

\begin{figure}[htpb]
  \centering
  \includegraphics[width=0.6\linewidth]{steering-vectors.png}
  \caption{Lenkung von 25 \%}
  \label{fig:lenkung}
\end{figure}

\begin{figure}[htpb]
  \centering
  \includegraphics[width=0.8\linewidth]{mittelwert-abweichung.png}
  \caption[Abweichung Mittelwert]{Prozentuale Abweichung vom Mittelwert}
  \label{fig:abweichung_mittelwert}
\end{figure}

Mit diesem Konzept konnte zwar keine lineare Steuerung realisiert werden, dafür ist die Auflösung
aber sehr fein. Wie in der Abbildung \namerefit{fig:abweichung_mittelwert} zu sehen ist, kann man
Radien von 7 cm (Radabstand / 2) bis zu 70 m fahren. Dank diesem grossen Spektrum kann beim Regeln
für grosse Abweichungen
sehr stark eingelenkt werden und bei kleinen Abweichungen mit feinster Auflösung korrigiert
werden. Dank dieser Steuerung ist die Regelung sehr stabil.

Der Radius errechnet sich mit der folgenden Formel: $R$ beschreibt den Radius, $V_{Av}$ die
Durchschnittsgeschwindigkeit, $d$ den Radabstand und $p$ den prozentuellen Steuereinschlag. Alle
Längenangaben sind in cm.

\begin{equation*}
  R = \frac{V_{Av}\times d}{2 \times p} \qquad R[cm]; V_{Av}\left[\frac{cm}{s}\right], d[cm], p[\%/100]
\end{equation*}

Um die jeweiligen Perioden-Zeiten $T_{left,right}$ zu berechnen wurde folgende Formel verwendet, wobei
$T_{Av}$ die gemeinsame Perioden-Zeit beschreibt und $p$ den Prozentsatz, um welchen gesteuert werden soll:

\begin{align*}
  V_{left,right} &= V_{Av} \times \frac{100 \pm p}{100} \\
  &= \frac{1}{T_{left,right}} \\
  &= \frac{1}{T_{Av}} \times \frac{100 \pm p}{100}
\end{align*}

\begin{align*}
  T_{left,right} = T_{Av} \times \frac{100}{100 \pm p}
\end{align*}


\paragraph{PID-Regler}
\label{par:pid_regler}

Damit das Fahrzeug beim Fahren auf seiner Spur bleibt, war es notwendig eine Regelung der
Geschwindigkeit beider Schrittmotoren zu implementieren. Diese Regelung wurde mittels eines
PID-Reglers gelöst. Ziel ist es, die Ausgangsgrösse $y(t)$ nach 0 zu regeln. Der Wert, welcher vom
Raspberry übergeben wird, wird negiert und dient dann als Eingangsgrösse (Abweichung) $e(t)$

\begin{figure}[htpb]
  \centering
  \includegraphics[width=0.8\linewidth]{pid-regulation.png}
  \caption{PID-Regler im System}
  \label{fig:pid-regulation}
\end{figure}

Die Regelung wurde gemäss folgender Gleichung umgesetzt.

\begin{equation*}
  u(t) = K_p \times e+ K_i \times \int_{\Delta t} e \times \Delta t + K_d \times
  \frac{\Delta e }{\Delta t}
\end{equation*}

Da die Regelung auf einem Mikrokontroller
umgesetzt wurde, können wir in diskreten Zeitabschnitten rechnen. Anstelle von $dt$ wird $\Delta t$
verwendet. Diese Zeitdifferenz entspricht hier 50 ms.

Die Ausgangs-Grösse des PID-Reglers stellt sich also aus der Summe von Proportional-, Integral-
und Differentialanteil zusammen.

\begin{description}
  \item[Proportionalanteil] die Eingangsgrösse wird um den Faktor $K_p$ verstärkt.
  \item[Integralanteil] der Fehler wird bei jedem Durchlauf aufsummiert und die Summe mit dem Faktor
    $K_i$ verstärkt.
  \item[Differentialanteil] hier wird die Änderungsrate aus der Differenz zwischen dem aktuellen
    und dem vorhergehenden Fehler berechnet und mit dem Faktor $K_d$ verstärkt. Als Zeitdifferenz
    $\Delta t$ gelten hier wieder die 50 ms.
\end{description}

Da mit dem Abfahren einer Strecke ein integrierendes Verhalten beschrieben wird, ist es nicht
notwendig, einen Integralanteil zu implementieren. Dazu kommt, dass durch das Messsystem ein
Totzeitglied in den Regelkreis eingefügt wird. Dieses Glied besitzt einen schnell abfallenden
Phasen-Gang bei hohen Frequenzen (hohen Geschwindigkeiten). Ein integrierendes Verhalten der
Regelung würde daher der Stabilität entgegen wirken. Bei Geschwindigkeiten unter 10 cm pro Sekunde
bemerkt man das Totzeitglied nicht und die Strecke kann mittels einfachem P-Regler stabilisiert
werden. Wird die Geschwindigkeit auf 12 cm pro Sekunde erhöht, so wird das System instabil. Um
das System auch bei höheren Geschwindigkeiten zu stabilisieren gibt es zwei Möglichkeiten:

\begin{description}
  \item[Totzeit verringern] Wartezeit beim Raspberry durch Aufteilung auf mehrere Threads/Cores.
    Nicht durchführbar, da ansonsten ein zeitlicher Engpass entstehen könnte.
  \item[Differentialanteil erhöhen] Mit einem Differentialanteil wird die Phasenlage um 90\textsuperscript{o}
    angehoben. Die Geschwindigkeit, bei welcher das System instabil wird, kann so erhöht werden.
\end{description}



